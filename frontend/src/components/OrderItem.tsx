import { Card, Container } from "react-bootstrap";
import { useShoppingCart } from "../context/ShoppingCartContext";
import storeItems from "../data/items.json";
import { formatCurrency } from "../utilities/formatCurrency";

type OrderItemProps = {
  id: number;
  client: string;
  items: {
    id: number;
    quantity: number;
  }[];
};

export function OrderItem({ id, client, items }: OrderItemProps) {
  const orderItems = items.map((item) => {
    const itemData = storeItems.find((storeItem) => storeItem.id === item.id);
    return {
      ...itemData,
      quantity: item.quantity,
    };
  });

  const total = orderItems.reduce((acc, item) => acc + 25 * item.quantity, 0);

  return (
    <Card className="mb-3">
      <Card.Header>
        <h5>Pedido {id}</h5>
      </Card.Header>
      <Card.Body>
        <Container className="mb-3">
          <h6 className="text-muted">Cliente: {client}</h6>
        </Container>
        <Container className="mb-3">
          {orderItems.map((item) => (
            <Card.Text key={item.id}>
              {item.name} - {item.quantity}x - {formatCurrency(item.price)}
            </Card.Text>
          ))}
        </Container>
        <Card.Text>
          <strong>Total: {formatCurrency(total)}</strong>
        </Card.Text>
      </Card.Body>
    </Card>
  );
}
