import { Container } from "react-bootstrap";

export function About() {
  return (
    <>
      <Container className="d-flex justify-content-center align-items-center">
        <h1 className="text-center">Informações adicionais</h1>
      </Container>
      <Container className="d-flex justify-content-center align-items-center">
        <h5 className="text-center">
          Projeto desenvolvido para a disciplina de Tópicos Especiais do curso
          de Análise e Desenvolvimento de Sistemas do Instituto Federal de
          Educação, Ciência e Tecnologia de São Paulo - Campus Jacareí.
        </h5>
      </Container>
      <Container className="d-flex justify-content-center align-items-center mt-5">
        <h5 className="text-center">
          Desenvolido por: <b>Iago Araújo</b>
        </h5>
      </Container>
    </>
  );
}
