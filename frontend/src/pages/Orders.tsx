import { Container } from "react-bootstrap";
import { useShoppingCart } from "../context/ShoppingCartContext";
import { OrderItem } from "../components/OrderItem";
import { formatCurrency } from "../utilities/formatCurrency";

const mockOrders = [
  {
    id: 1,
    client: "João",
    items: [
      {
        id: 1,
        quantity: 1,
      },
      {
        id: 2,
        quantity: 2,
      },
    ],
  },
];

export function Orders() {
  return (
    <>
      <Container className="d-flex justify-content-center align-items-center">
        <h1 className="text-center">Pedidos</h1>
      </Container>
      <Container className="d-flex justify-content-center align-items-center">
        <h4 className="text-center">Pedidos realizados.</h4>
      </Container>

      <Container>
        {mockOrders.map((order) => (
          <OrderItem
            key={order.id}
            id={order.id}
            client={order.client}
            items={order.items}
          />
        ))}
      </Container>
    </>
  );
}
