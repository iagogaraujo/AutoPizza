import { Button, Container } from "react-bootstrap";

export function Error() {
  return (
    <>
      <Container className="d-flex justify-content-center align-items-center">
        <h1 className="text-center">Erro 404</h1>
      </Container>
      <Container className="d-flex justify-content-center align-items-center">
        <h4 className="text-center">Página não encontrada.</h4>
      </Container>
      <Container className="d-flex justify-content-center align-items-center mt-3">
        <Button
          className="btn btn-secondary shadow-sm"
          size="md"
          onClick={() => {
            window.location.href = "/";
          }}
        >
          Voltar
        </Button>
      </Container>
    </>
  );
}
