import { Button, Container } from "react-bootstrap";

export function Home() {
  return (
    <>
      <Container className="d-flex justify-content-center align-items-center">
        <h1 className="text-center">Bem vindo ao AutoPizza!</h1>
      </Container>
      <Container className="d-flex justify-content-center align-items-center">
        <h4 className="text-center">
          Um sistema totalmente automatizado para preparo de pizzas.
        </h4>
      </Container>
      {/* Image */}
      <Container className="d-flex justify-content-center align-items-center">
        <img
          src="/imgs/image_home.png"
          alt="Pizza"
          width="500px"
          height="500px"
        />
      </Container>
      <Container className="d-flex justify-content-center align-items-center">
        <h4 className="text-center">
          Pronto para começar? Clique no botão abaixo para explorar nosso
          cardápio.
        </h4>
      </Container>
      <Container className="d-flex justify-content-center align-items-center mt-3">
        {/* <button
          className="btn btn-secondary shadow-sm"
          size="lg"
          onClick={() => (window.location.href = "/store")}
        >
          Cardápio
        </button> */}
        <Button
          className="btn btn-secondary shadow-sm"
          size="lg"
          onClick={() => {
            window.location.href = "/store";
          }}
        >
          Cardápio
        </Button>
      </Container>
    </>
  );
}
