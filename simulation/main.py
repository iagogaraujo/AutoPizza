# Simulação do AutoPizza - Um sistema de entrega de pizzas somente com agentes artificiais, sem humanos

# Definição de Agentes
# ------------------------------------------------------------
# Agente Atendente
# Funções
# - Receber pedido
# ------------------------------------------------------------
# Agente Cozinheiro
# Funções
# - Preparar pedido
# ------------------------------------------------------------
# Agente Entregador
# Funções
# - Entregar pedido
# - Receber pagamento
# ------------------------------------------------------------
# Cliente
# Funções
# - Fazer pedido
# - Receber pedido
# - Pagar pedido
# ------------------------------------------------------------

# Importação de bibliotecas
import random
import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import simpy

# Definição de variáveis
# ------------------------------------------------------------
TEMPO_SIMULACAO = 1000
TEMPO_PREPARO = 10
TEMPO_ENTREGA = 10
TAMANHO_FILA = 15
PRECO_PIZZA = 20
QUANTIDE_CLIENTES = 100
QUANTIDADE_ATENDENTES = 2
QUANTIDADE_COZINHEIROS = 2
QUANTIDADE_ENTREGADORES = 2
# ------------------------------------------------------------
# Criando ambiente de simulação
# ------------------------------------------------------------
env = simpy.Environment()
# Criando fila de espera
fila = simpy.FilterStore(env, capacity=TAMANHO_FILA)

# Criando agentes
# ------------------------------------------------------------


class Atendente(object):
    def __init__(self, env, name, fila):
        self.env = env
        self.name = name
        self.fila = fila
        self.action = env.process(self.run())

    def run(self):
        while True:
            # Recebe pedido
            pedido = yield self.fila.get()
            print('Atendente %s recebeu pedido %s' % (self.name, pedido))
            # Prepara pedido
            yield self.env.timeout(TEMPO_PREPARO)
            print('Atendente %s preparou pedido %s' % (self.name, pedido))
            # Envia pedido para cozinha
            cozinha.put(pedido)


class Cozinheiro(object):
    def __init__(self, env, name, cozinha):
        self.env = env
        self.name = name
        self.cozinha = cozinha
        self.action = env.process(self.run())

    def run(self):
        while True:
            # Recebe pedido
            pedido = yield self.cozinha.get()
            print('Cozinheiro %s recebeu pedido %s' % (self.name, pedido))
            # Prepara pedido
            yield self.env.timeout(TEMPO_PREPARO)
            print('Cozinheiro %s preparou pedido %s' % (self.name, pedido))
            # Envia pedido para entrega
            entrega.put(pedido)


class Entregador(object):
    def __init__(self, env, name, entrega):
        self.env = env
        self.name = name
        self.entrega = entrega
        self.action = env.process(self.run())

    def run(self):
        while True:
            # Recebe pedido
            pedido = yield self.entrega.get()
            print('Entregador %s recebeu pedido %s' % (self.name, pedido))
            # Entrega pedido
            yield self.env.timeout(TEMPO_ENTREGA)
            print('Entregador %s entregou pedido %s' % (self.name, pedido))
            # Recebe pagamento
            pagamento.put(pedido)


class Cliente(object):
    def __init__(self, env, name, fila):
        self.env = env
        self.name = name
        self.fila = fila
        self.action = env.process(self.run())

    def run(self):
        # Faz pedido
        pedido = 'Pedido %s' % self.name
        print('Cliente %s fez pedido %s' % (self.name, pedido))
        # Envia pedido para atendente
        yield self.fila.put(pedido)
        # Recebe pedido
        pedido = yield pagamento.get()
        print('Cliente %s recebeu pedido %s' % (self.name, pedido))
        # Paga pedido
        print('Cliente %s pagou pedido %s' % (self.name, pedido))
        # Tempo de espera para fazer novo pedido
        yield self.env.timeout(random.randint(1, 10))


# Criando recursos
# ------------------------------------------------------------
cozinha = simpy.Store(env)
entrega = simpy.Store(env)
pagamento = simpy.Store(env)

# Criando agentes
# ------------------------------------------------------------
for i in range(QUANTIDADE_ATENDENTES):
    Atendente(env, 'Atendente %d' % i, fila)

for i in range(QUANTIDADE_COZINHEIROS):
    Cozinheiro(env, 'Cozinheiro %d' % i, cozinha)

for i in range(QUANTIDADE_ENTREGADORES):
    Entregador(env, 'Entregador %d' % i, entrega)

for i in range(QUANTIDE_CLIENTES):
    Cliente(env, 'Cliente %d' % i, fila)

# Iniciando simulação
# ------------------------------------------------------------
env.run(until=TEMPO_SIMULACAO)
